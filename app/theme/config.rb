images_dir = "icons/png"
generated_images_dir = "icons/png/_"


#################################
asset_cache_buster :none

# Make a copy of sprites with a name that has no uniqueness of the hash.
on_sprite_saved do |filename|
  if File.exists?(filename)
    FileUtils.cp filename, filename.gsub(%r{-s[a-z0-9]{10}\.png$}, '.png')
  end
end

# Replace in stylesheets generated references to sprites
# by their counterparts without the hash uniqueness.
on_stylesheet_saved do |filename|
  if File.exists?(filename)
    css = File.read filename
    File.open(filename, 'w+') do |f|
      f << css.gsub(%r{-s[a-z0-9]{10}\.png}, '.png')
    end
  end
end

module Compass::SassExtensions::Functions::Sprites
    def sprite_path(map)
      Sass::Script::String.new(map.filename)
    end
    Sass::Script::Functions.declare :sprite_path, [:map]
end