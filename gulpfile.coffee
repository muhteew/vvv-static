gulp = require('gulp')
$ = require('gulp-load-plugins')()
merge = require('merge-stream')
runSequence = require('run-sequence')
pagespeed = require('psi')
path = require('path')

buildInfo = null

gulp.task 'build:clean', ->
  gulp.src('app/.build', read: false).pipe $.clean()

gulp.task 'build:config', ->
  env = process.env.VVV_ENV || "dev"
  console.log 'Compile config for env [' + env + ']'
  envConfig = require('./config/environments/' + env + '.json')
  config = JSON.stringify(envConfig)
  gulp.src('config/vvv.config.js')
  .pipe($.replace('__insert__', config))
  .pipe($.concat("vvv.config.ts"))
  .pipe gulp.dest('app/src')

#create static angular templates for each module
gulp.task 'build:static-templates', ->
  merged = merge()
  #TODO read from source folder !
  modules = [
    'global'
    'user'
    'posts'
    'feed'
    'popular'
    'profiles'
    'comments'
    'media'
  ]
  i = 0
  while i < modules.length
    module = modules[i]
    merged.add gulp.src('app/src/vvv.' + module + '/directives/**/*.html').pipe($.angularTemplatecache(
      filename: 'vvv.' + module + '-static-directives.js'
      module: 'vvv.' + module
      root: 'src/vvv.' + module + '/directives')).pipe(gulp.dest('app/.build/static-templates'))

    merged.add gulp.src('app/src/vvv.' + module + '/views/**/*.html').pipe($.angularTemplatecache(
      filename: 'vvv.' + module + '-static-views.js'
      module: 'vvv.' + module
      root: 'src/vvv.' + module + '/views')).pipe(gulp.dest('app/.build/static-templates'))

    merged.add gulp.src('app/src/vvv.' + module + '/modals/**/*.html').pipe($.angularTemplatecache(
      filename: 'vvv.' + module + '-static-modals.js'
      module: 'vvv.' + module
      root: 'src/vvv.' + module + '/modals')).pipe(gulp.dest('app/.build/static-templates'))
    i++
  merged

#build typescript
gulp.task 'build:ts', ->
  files = [
    'app/src/vvv.global/global-module.ts'
    'app/src/vvv.global/**/*.ts'
    'app/src/vvv.user/user-module.ts'
    'app/src/vvv.user/**/*.ts'
    'app/src/vvv.posts/posts-module.ts'
    'app/src/vvv.posts/**/*.ts'
    'app/src/vvv.popular/popular-module.ts'
    'app/src/vvv.popular/**/*.ts'
    'app/src/vvv.feed/feed-module.ts'
    'app/src/vvv.feed/**/*.ts'
    'app/src/vvv.profiles/profiles-module.ts'
    'app/src/vvv.profiles/**/*.ts'
    'app/src/vvv.comments/comments-module.ts'
    'app/src/vvv.comments/**/*.ts'
    'app/src/vvv.media/media-module.ts'
    'app/src/vvv.media/**/*.ts'
    'app/src/vvv.config.ts'
    'app/src/vvv/app-faker.ts'
    'app/src/vvv/**/*.ts'
  ]

  gulp.src(files, { base: 'app/src'  })
  .pipe($.plumber())
  .pipe($.sourcemaps.init())
  .pipe($.typescript(sortOutput : true, declarationFiles: true))
  .js.pipe($.concat('app.js'))
  .pipe($.sourcemaps.write(".", {includeContent: true}))
  .pipe gulp.dest('app/.build')


#build js sources
gulp.task 'build:babel', ->
  files = [
    'app/src/vvv.global/global-module.js'
    'app/src/vvv.global/**/*.js'
    'app/src/vvv.user/user-module.js'
    'app/src/vvv.user/**/*.js'
    'app/src/vvv.posts/posts-module.js'
    'app/src/vvv.posts/**/*.js'
    'app/src/vvv.popular/popular-module.js'
    'app/src/vvv.popular/**/*.js'
    'app/src/vvv.feed/feed-module.js'
    'app/src/vvv.feed/**/*.js'
    'app/src/vvv.config.js'
    'app/src/vvv/**/*.js'
  ]
  gulp.src(files)
  .pipe($.plumber())
  .pipe($.sourcemaps.init())
  .pipe($.concat('app.js'))
  .pipe($.babel())
  .pipe($.sourcemaps.write('.'))
  .pipe gulp.dest('app/.build')

#concat external libs
gulp.task 'build:bower-libs', ->
  gulp.src([
    'app/bower_components/jquery/dist/jquery.min.js'
    'app/bower_components/angular/angular.js'
    'app/bower_components/angular-ui-router/release/angular-ui-router.js'
    'app/bower_components/angular-animate/angular-animate.js'
    'app/bower_components/angular-cache/dist/angular-cache.min.js'
    'app/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.min.js'
    'app/bower_components/ng-file-upload/angular-file-upload.min.js'
    'app/bower_components/ng-file-upload/angular-file-upload-shim.min.js'
    'app/libs/ui-bootstrap-modal/ui-bootstrap-custom-0.12.1.min.js'
    'app/libs/ui-bootstrap-modal/ui-bootstrap-custom-tpls-0.12.1.min.js'
    'app/libs/headroom.js'
    'app/bower_components/angular-strap/dist/modules/dimensions.min.js'
    'app/bower_components/angular-strap/dist/modules/tooltip.min.js'
    'app/bower_components/angular-strap/dist/modules/tooltip.tpl.min.js'
    'app/bower_components/angular-strap/dist/modules/popover.min.js'
    'app/bower_components/angular-strap/dist/modules/popover.tpl.min.js'
    'app/bower_components/ngSticky/dist/sticky.min.js',
    'app/bower_components/angular-strap/dist/modules/debounce.min.js'
    'app/bower_components/angular-strap/dist/modules/scrollspy.min.js',
    'app/bower_components/angular-strap/dist/modules/collapse.min.js'
  ])
  .pipe($.concat('bower-libs-all.js'))
  .pipe gulp.dest('app/.build')

#build all resources but not prepare them for particular environment
gulp.task 'build:all', [
  'build:ts'
  'build:bower-libs'
  'build:static-templates'
]

gulp.task 'rebuild', (done) ->
  runSequence 'build:config', 'build:clean', 'build:all', done

gulp.task 'getBuildInfo', ->

  $.git.exec {args : 'log -n 1'}, (err, stdout) ->
    if err then throw err
    buildInfo = {version : require("./package.json").version, commit : stdout}

###
Build stage version - concat src, libs & copy to `.build/stage`
The main purpose of this build is to deploy project where vendor libraries are separated from
source application code in order to be able debug sources when necessary.
This build have contcacinated vendor libs separated from compiled sources (with maps).
###
gulp.task 'rebuild:stage', ['getBuildInfo', 'rebuild'], ->

  if process.env.VVV_ENV != "stage"
    console.warn "VVV_ENV is not `stage` is it intended ?"    
  
  merged = merge()
  
  #copy app.js & app.js.map
  merged.add gulp.src([
    'app/.build/app.js'
    'app/.build/static-templates/**/*.js'
  ]).pipe($.concat('app.js')).pipe(gulp.dest('app/.build/stage'))
  merged.add gulp.src('app/.build/app.js.map').pipe($.concat('app.js.map')).pipe(gulp.dest('app/.build/stage'))
  
  #libs
  merged.add gulp.src('app/.build/bower-libs-all.js').pipe($.concat('libs.js')).pipe(gulp.dest('app/.build/stage'))
  
  #copy css
  #merged.add gulp.src('app/bower_components/bootstrap/dist/css/bootstrap.css').pipe($.concat('bootstrap.css')).pipe(gulp.dest('app/.build/stage'))
  merged.add gulp.src('app/css/*.css').pipe(gulp.dest('app/.build/stage/css'))
  
  #copy fonts
  merged.add gulp.src('app/css/fonts/*').pipe(gulp.dest('app/.build/stage/css/fonts'))

  #copy img
  merged.add gulp.src('app/img/*').pipe(gulp.dest('app/.build/stage/img'))

  #get buildInfo
  #buildInfoStr = "ver: #{buildInfo.version}\n#{buildInfo.commit.replace(/^\s*[\r\n]/gm,"", '')}"
  buildInfoStr = "<script type='text/javascript'>console.log(" + JSON.stringify(buildInfo, null, 2) + ");</script>"

  #buld index.html  
  merged.add gulp.src('app/index.html').pipe($.htmlReplace(
    'info' : buildInfoStr
    'js': [
      'libs.js'
      'app.js'
    ]
    'css': [
      'css/style.css'
    ])).pipe(gulp.dest('app/.build/stage'))
  
  merged


#build for dev, and then run watcher
gulp.task 'default', [
  'build:all'
  'watch'
]

#Build src and then reload browser
gulp.task 'build-reload', [ 'build:all' ], ->
  gulp.src('app/.build/app.js').pipe $.livereload()

#Watch for changes and then rebuild source
gulp.task 'watch', ->
  $.livereload.listen()
  gulp.watch 'app/**/*', [ 'build-reload' ]


#Work only for public networks https://groups.google.com/forum/#!topic/pagespeed-insights-discuss/OrLMYreBNd8
# Run PageSpeed Insights
# Update `url` below to the public URL for your site
gulp.task 'pagespeed', (cb) ->
  # Update the below URL to the public URL of your site
  pagespeed.output 'veevavoo.dev', { strategy: 'desktop' }, cb

#Compile sass
gulp.task 'sass', ->
  gulp.src(['app/theme/styles/*.scss', '!app/theme/styles/icons.scss', '!app/theme/styles/_sprite-generator.scss'])
  .pipe($.sourcemaps.init())
  .pipe($.sass())
  .pipe($.sourcemaps.write('maps'))
  .pipe(gulp.dest('app/theme/styles/_'))
  .pipe($.livereload())

#Make png sprites
gulp.task 'compass', ->
  gulp.src(['app/theme/styles/_sprite-generator.scss',  'app/theme/styles/icons.scss'])
  .pipe(
    $.compass
      config_file: path.join(process.cwd(), '/app/theme/config.rb'),
      project: path.join(process.cwd(), '/app/theme'),
      css: 'styles/_',
      sass: 'styles'
  )
  .pipe(gulp.dest('app/theme/styles/_'))
  .pipe($.livereload())

#Make base64 svg sprites
gulp.task 'iconify', ->
  $.iconify({
    src: 'app/theme/icons/svg/*.svg',
    pngOutput: 'app/theme/icons/svg/_/png',
    scssOutput: 'app/theme/icons/svg/_',
    cssOutput: 'app/theme/icons/svg/_',
    styleTemplate: 'app/theme/iconify.hbs',
    defaultWidth: '32px',
    defaultHeight: '32px',
    svgoOptions: {
      enabled: true,
      options: {
        plugins: [
          { removeUnknownsAndDefaults: false },
          { mergePaths: false }
        ]
      }
    }
  })

#Build theme
gulp.task 'build:theme',['iconify'], (done) ->
  runSequence 'sass', done

#Watch for theme changes
gulp.task 'watch:theme', ->
  $.livereload.listen()
  gulp.watch(['temp.html', '*.php', 'twig-templates/**/*.twig']).on('change', $.livereload.changed)
  gulp.watch ['app/theme/icons/svg/*', 'app/theme/iconify.hbs'], ['build:theme']
  gulp.watch ['app/theme/styles/*.scss',  '!app/theme/styles/icons.scss', '!app/theme/styles/_sprite-generator.scss'], [ 'sass' ]
  gulp.watch ['app/theme/styles/_sprite-generator.scss', 'app/theme/styles/icons.scss'], [ 'compass' ]

